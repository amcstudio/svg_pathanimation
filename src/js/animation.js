'use strict';
~ function () {
    var $ = TweenMax,
    path = document.getElementById('path'),
    points = MorphSVGPlugin.pathDataToBezier(path);

    window.init = function () {
        
       var tl = new TimelineMax();
       tl.to('#baloon',0.5,{opacity:1, ease:Power2.easeInOut})
       tl.to('#baloon',10,{bezier:{type:"cubic",values:points},force3D:true, ease:Power1.easeInOut},0)
       
    }
   
}();
